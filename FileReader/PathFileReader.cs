﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReader
{
    public static class PathFileReader
    {
        public static string ReadInput()
        {
            Console.WriteLine("Введите адрес папки с файлами");
            while (true)
            {
                string path = Console.ReadLine();
                if (IsPathOk(path))
                {
                    return path;
                }
                Console.WriteLine("Некорректный путь, попробуйте другой");
            } 
        }
        private static bool IsPathOk(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            return false;
        }
    }
}
