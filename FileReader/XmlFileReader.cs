﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FileReader
{
    public class XmlFileReader : FileReader
    {
        const string RootName = "Values";
        const string NodeName = "Value";

        private XDocument _doc;

        public XmlFileReader(string filePath)
        {
            _doc = XDocument.Load(filePath);
        }
        public override void ReadFile(Dictionary<string, int> symbolDictionary)
        {
            var listXElements = _doc.Root.Descendants(RootName).First().Descendants(NodeName);

            foreach (XElement listElement in listXElements)
            {
                if (listElement.Name == NodeName)
                {
                    AddToDictionary(symbolDictionary, listElement.Value);
                }
            }
        }
    }
}
