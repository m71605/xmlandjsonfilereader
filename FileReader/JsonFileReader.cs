﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace FileReader
{
    public class JsonFileReader : FileReader
    {
        private string _jsonString;

        public JsonFileReader(string filePath)
        {
            _jsonString = File.ReadAllText(filePath);
        }
        public override void ReadFile(Dictionary<string, int> symbolDictionary)
        {
            var jsonValues = JsonSerializer.Deserialize<JsonValues>(_jsonString);

            foreach (string element in jsonValues.Values)
            {
                AddToDictionary(symbolDictionary, element);
            }
            
        }

       
    }
}
