﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReader
{
    public abstract class FileReader
    {
        public abstract void ReadFile(Dictionary<string, int> symbolDictionary);

        protected void AddToDictionary (Dictionary<string, int> symbolDictionary, string element)
        {
            if (symbolDictionary.ContainsKey(element))
            {
                symbolDictionary[element] += 1;
            }
            else
            {
                symbolDictionary.Add(element, 1);
            }
        }
    }
}
