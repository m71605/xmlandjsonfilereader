﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReader
{

    public class AllFilesReader
    {
        const string JsonFormatExtension = ".json";
        const string XmlFormatExtension = ".xml";

        private string _folderPath;
        public AllFilesReader(string folderPath)
        {
            _folderPath = folderPath;
        }
        public void ReadAllFromDirectory(Dictionary<string, int> symbolDictionary)
        {
            string[] fileEntries = Directory.GetFiles(_folderPath);
            
            foreach (string fileName in fileEntries)
            {   
                var fileExtinsion = ReconizeFileExtension(out FileReader fileReader, fileName);
                if (fileExtinsion == FileExtension.UNKNOWN) break;
                fileReader.ReadFile(symbolDictionary);
            }
        }

        private FileExtension ReconizeFileExtension(out FileReader fileReader, string filePath)
        {
            string fileExtension = filePath.Substring(filePath.IndexOf("."));
            
            fileReader = null;
            switch (fileExtension)
            {
                case JsonFormatExtension:
                    fileReader = new JsonFileReader(filePath);
                    return FileExtension.XML;
                case XmlFormatExtension:
                    fileReader = new XmlFileReader(filePath);
                    return FileExtension.JSON;
                default:
                    return FileExtension.UNKNOWN;
            }

        }
    }
}
