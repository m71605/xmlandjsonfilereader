﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FileReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> symbolDictionary = new Dictionary<string, int>();
            string folderPath = PathFileReader.ReadInput();

            AllFilesReader allFilesReader = new AllFilesReader(folderPath);
            allFilesReader.ReadAllFromDirectory(symbolDictionary);
            
            if (symbolDictionary.Count>0)
            {
                int maxFriquency = GetMaxFriquency(symbolDictionary);
                string maxFriquentlySymbol = GetMaxFriquentlySymbol(symbolDictionary, maxFriquency);
                Console.WriteLine("Чаще всего встречается символ \"" + maxFriquentlySymbol + "\" - " + Convert.ToString(maxFriquency) + " раз(а)");
            }
            else
            {
                Console.WriteLine("Файлы не обнаружены или пусты");
            }
           
            Console.ReadLine();
        }

        private static int GetMaxFriquency(Dictionary<string, int> symbolDictionary)
        {
            return symbolDictionary.Values.Max();
        }
        private static string GetMaxFriquentlySymbol(Dictionary<string, int> symbolDictionary, int maxFriquency)
        {
            return symbolDictionary.FirstOrDefault(x => x.Value == maxFriquency).Key;
        }
    }
}
